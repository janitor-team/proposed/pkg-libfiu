libfiu (1.1-1) unstable; urgency=medium

  * New upstream release.
  * Install docs directly into /usr/share/doc/libfiu-dev, not in a doc/
    subdirectory.
  * Drop 0001-Don-t-build-Python-2.x-bindings-used-in-tests.patch; Python 2
    support dropped upstream.
  * Drop 0002-bindings-python-Increment-refcount-on-failinfo-objec.patch;
    applied upstream.
  * Bump Standards-Version to 4.6.0.
  * Rename a lintian tag.

 -- Chris Lamb <lamby@debian.org>  Sat, 27 Nov 2021 10:00:13 -0800

libfiu (1.00-7) unstable; urgency=medium

  * Fix FTBFS on amd64 with Python 3.8. Thanks, Alberto. (Closes: #954287)
  * Bump Standards-Version to 4.5.0.

 -- Chris Lamb <lamby@debian.org>  Sat, 28 Mar 2020 23:11:55 +0000

libfiu (1.00-6) unstable; urgency=medium

  * Drop Build-Depends on libpython-all-dev for Python 2.x removal. Thanks,
    Sandro Tosi. (Closes: #936856)

 -- Chris Lamb <lamby@debian.org>  Sun, 19 Jan 2020 09:29:59 +0000

libfiu (1.00-5) unstable; urgency=medium

  * Don't run the non-deterministic "parallel" and "parallel-wildcard" tests.

 -- Chris Lamb <lamby@debian.org>  Sat, 28 Dec 2019 16:31:00 +0000

libfiu (1.00-4) unstable; urgency=medium

  * Don't FTBFS when multiple Python version libraries exist in build tree by
    manually deleting all but the version for the default Python version
    returned by py3versions prior to running the test suite. (Closes: #944911)
  * Specify Rules-Requires-Root: no.
  * Bump Standards-Version to 4.4.1.

 -- Chris Lamb <lamby@debian.org>  Sat, 23 Nov 2019 17:26:03 -0500

libfiu (1.00-3) unstable; urgency=medium

  * Patch the upstream Makefile to not build the Python 2.x bindings to ensure
    the tests pass.

 -- Chris Lamb <lamby@debian.org>  Tue, 10 Sep 2019 11:45:27 +0100

libfiu (1.00-2) unstable; urgency=medium

  * Drop Python 2.x support in the autopkgtests.

 -- Chris Lamb <lamby@debian.org>  Tue, 10 Sep 2019 11:00:10 +0100

libfiu (1.00-1) unstable; urgency=medium

  * New upstream release.
    - Drop all patches; applied upstream.
  * Drop Python 2.x packages. (Closes: #936856)
  * Register HTML documentation with doc-base.
  * Update Lintian overrides:
    - Override pkg-config-unavailable-for-cross-compilation Lintian warning for
      usr/lib/pkgconfig/libfiu.pc; the library is for runtime fault injection
      and thus cross-compilation is not a use-case.
    - Drop misnamed and thus unused debian/libfiu.lintian.override.
  * Drop debian/source/options.
  * Bump Standards-Version to 4.4.0.

 -- Chris Lamb <lamby@debian.org>  Tue, 10 Sep 2019 10:30:40 +0100

libfiu (0.98-2) unstable; urgency=medium

  * Honour CPPFLAGS and LDFLAGS when building shared libraries to ensure
    hardening is applied to generated objects.
  * Add patch from upstream to avoid the use of PATH_MAX for GNU Hurd.
  * debian/rules:
    - Indent consistently.
    - Enable hardening build flags.
  * debian/control:
    - Move to Standards-Version 4.3.0.
    - Move to debhelper-compat level 12.

 -- Chris Lamb <lamby@debian.org>  Tue, 08 Jan 2019 08:21:43 +0100

libfiu (0.98-1) unstable; urgency=medium

  * New upstream release.
    - Drop patches; applied upstream.
  * Add "Build-Depends-Package" field to the symbols file.

 -- Chris Lamb <lamby@debian.org>  Thu, 13 Dec 2018 23:12:37 +0100

libfiu (0.97-2) unstable; urgency=medium

  * Mangle the return offset size for 64-bit variants too to prevent FTBFS on
    32-bit architectures. (Closes: #911733)

 -- Chris Lamb <lamby@debian.org>  Wed, 24 Oct 2018 23:17:19 -0400

libfiu (0.97-1) unstable; urgency=medium

  * New upstream release.
    - Drop all patches; applied upstream.
    - Update debian/symbols.
  * Use debhelper-compat (= 11) build-dependency and drop debian/compat.

 -- Chris Lamb <lamby@debian.org>  Tue, 23 Oct 2018 19:27:05 -0400

libfiu (0.96-5) unstable; urgency=medium

  * Apply patch from upstream to write fiu_ctrl.py atomically to avoid FTBFS
    when building in parallel. (Closes: #909843)
  * Bump Standards-Version to 4.2.1.

 -- Chris Lamb <lamby@debian.org>  Mon, 01 Oct 2018 22:04:49 +0100

libfiu (0.96-4) unstable; urgency=medium

  * Apply upstream patch to make the build more robust against --as-needed.
    (Closes: #902363)

 -- Chris Lamb <lamby@debian.org>  Sat, 30 Jun 2018 20:55:15 +0100

libfiu (0.96-3) unstable; urgency=medium

  * Update Vcs-* to point to salsa.debian.org.
  * Bump Standards-Version to 4.1.4

 -- Chris Lamb <lamby@debian.org>  Fri, 08 Jun 2018 17:52:39 +0100

libfiu (0.96-2) unstable; urgency=medium

  * Apply patch from upstream to make the build reproducible. (Closes: #894776)

 -- Chris Lamb <lamby@debian.org>  Wed, 04 Apr 2018 09:36:48 +0100

libfiu (0.96-1) unstable; urgency=medium

  * New upstream release. (Closes: #893049)
    - Fix FTBFS due to build/test parellelism. (Closes: #893049)
  * Add explicit python3-distutils Build-Dependency.

 -- Chris Lamb <lamby@debian.org>  Sun, 25 Mar 2018 20:32:20 -0400

libfiu (0.95-7) unstable; urgency=medium

  * Drop debian/patches/0001-ALso-preload-libfiu.so-explicitly.patch; it does
    not actually fix #893049 -- something else/strange is going on.

 -- Chris Lamb <lamby@debian.org>  Sat, 17 Mar 2018 18:38:49 -0400

libfiu (0.95-6) unstable; urgency=medium

  * LD_PRELOAD libfiu.so explicitly to avoid test failure. (Closes: #893049)

 -- Chris Lamb <lamby@debian.org>  Thu, 15 Mar 2018 19:05:21 -0700

libfiu (0.95-5) unstable; urgency=medium

  * Update Build-Depends for cross-compilation. Thanks to Helmut Grohne for
    the patch. (Closes: #892946)
  * Add gbp.conf.
  * Use .travis.yml from travis.debian.net.
  * Update debian/watch to use HTTPS URI.
  * wrap-and-sort -sa.

 -- Chris Lamb <lamby@debian.org>  Wed, 14 Mar 2018 12:16:55 -0700

libfiu (0.95-4) unstable; urgency=medium

  * Apply patch from Steve Langasek via Ubuntu to fix autopkgtests by using
    double quotes to survive the parser and by setting allow-stderr to account
    for the error message from ls. (Closes: #869709)

 -- Chris Lamb <lamby@debian.org>  Tue, 25 Jul 2017 21:51:44 +0100

libfiu (0.95-3) unstable; urgency=medium

  * Use /bin/ls over "ls" in autopkgtest.

 -- Chris Lamb <lamby@debian.org>  Fri, 18 Nov 2016 09:47:22 +0100

libfiu (0.95-2) unstable; urgency=medium

  * Expand the autopkgtest tests from upstram's suggestions.

 -- Chris Lamb <lamby@debian.org>  Thu, 17 Nov 2016 18:44:36 +0100

libfiu (0.95-1) unstable; urgency=medium

  * New upstream release.
  * All patches now applied upstream.
  * Move to DEP-5 debian/copyright format.

 -- Chris Lamb <lamby@debian.org>  Thu, 17 Nov 2016 16:10:15 +0100

libfiu (0.94-10) unstable; urgency=medium

  * Add a simple autopkgtest smoke test.

 -- Chris Lamb <lamby@debian.org>  Sun, 13 Nov 2016 10:09:27 +0000

libfiu (0.94-9) unstable; urgency=medium

  * Run "gbp pq import && gpq pq export" to refresh patches.
  * Add 0003-Sort-modules-to-ensure-reproducible-order-in-binary.patch to try
    to ensure a reproducible build for modules.

 -- Chris Lamb <lamby@debian.org>  Sun, 23 Oct 2016 13:30:26 +0200

libfiu (0.94-8) unstable; urgency=medium

  * Fix FTBFS under bash due to lack of '&&' in debian/rules.

 -- Chris Lamb <lamby@debian.org>  Thu, 20 Oct 2016 16:29:51 +0200

libfiu (0.94-7) unstable; urgency=medium

  * Actually drop /usr/lib/libfiu.so symlink in libfiu0 package.
    (Closes: #828828)

 -- Chris Lamb <lamby@debian.org>  Wed, 29 Jun 2016 14:59:41 +0200

libfiu (0.94-6) unstable; urgency=medium

  * Drop /usr/lib/libfiu.so symlink in libfiu0 package and add override.
    (Closes: #828828)

 -- Chris Lamb <lamby@debian.org>  Tue, 28 Jun 2016 10:51:01 +0200

libfiu (0.94-5) unstable; urgency=medium

  * Bump Standards-Version to 3.9.8.
  * Install /usr/lib/libfiu.so symlink to appease lintian.

 -- Chris Lamb <lamby@debian.org>  Mon, 27 Jun 2016 17:32:42 +0200

libfiu (0.94-4) unstable; urgency=medium

  * wrap-and-sort -sa.
  * Add debian/patches/0002-ld-as-needed.patch from Logan Rosen
    <logan@ubuntu.com>: Move libraries after files that
    need them to fix FTBFS with ld --as-needed. (Closes: #819373)
  * Move to HTTPS URI for Vcs-Git.

 -- Chris Lamb <lamby@debian.org>  Mon, 28 Mar 2016 23:48:24 +0400

libfiu (0.94-3) unstable; urgency=medium

  * Pass LC_ALL=C when running dh_auto_test to avoid FTBFS under exotic locales
    (eg. fr_CH.UTF-8).

 -- Chris Lamb <lamby@debian.org>  Tue, 22 Sep 2015 15:54:19 +0100

libfiu (0.94-2) unstable; urgency=medium

  * Don't hide compiler warnings by passing V=1 to upstream Makefiles.
  * Rename buildvers -> PYTHON2_VERSIONS, etc.
  * Apply upstream patch to fix FTBFS in tests on amd64.

 -- Chris Lamb <lamby@debian.org>  Sun, 16 Aug 2015 13:42:18 +0200

libfiu (0.94-1) unstable; urgency=medium

  * New upstream version.
  * Bump Standards-Version to 3.9.6.
  * Bump Debhelper compatibility level to 9.
  * wrap-and-sort.
  * Drop 01-fix-ftbfs-on-i386.diff - applied upstream.
  * Add dh-python to Build-Depends.
  * Update debian/symbols.

 -- Chris Lamb <lamby@debian.org>  Sat, 15 Aug 2015 10:57:19 +0200

libfiu (0.93-2) unstable; urgency=low

  * Add patch from upstream to fix FTBFS on i386.

 -- Chris Lamb <lamby@debian.org>  Wed, 30 Jul 2014 10:44:42 +0100

libfiu (0.93-1) unstable; urgency=medium

  * New upstream release.
  * Strip Debian revision from symbols.

 -- Chris Lamb <lamby@debian.org>  Tue, 29 Jul 2014 07:43:46 +0100

libfiu (0.92-1) unstable; urgency=low

  * New upstream release.
  * Add "this work is to be considered Public Domain" to debian/copyright.
    Thanks to Ansgar Burchardt.
  * Bump Standards-Version to 3.9.4.
  * Update debian/symbols.

 -- Chris Lamb <lamby@debian.org>  Wed, 30 Oct 2013 11:20:24 +0000

libfiu (0.91-2) unstable; urgency=low

  * Require the testsuite to pass at request of upstream - was optional since
    0.90-3.
  * Add/fix support for python3:
    - Add python3-fiu binary to debian/control
    - Adjust debian/rules to build for all versions
    - Change python/python3 build deps to python{3}-all-dev to support build
      for all versions
    Thanks to Scott Kitterman <debian@kitterman.com>.
  * Move to dh_python2. Thanks to Scott Kitterman <debian@kitterman.com>.

 -- Chris Lamb <lamby@debian.org>  Thu, 23 May 2013 00:49:47 +0100

libfiu (0.91-1) unstable; urgency=low

  * New upstream release.
  * Update symbols.
  * Drop 01_wrap_python_for_tests.diff - applied upstream.
  * Drop 02_include_stdlib.diff - applied upstream.

 -- Chris Lamb <lamby@debian.org>  Thu, 06 Sep 2012 02:29:33 +0100

libfiu (0.90-3) unstable; urgency=low

  * Make passing the testsuite optional; upstream does not 100% rely on it yet
    on all architectures.

 -- Chris Lamb <lamby@debian.org>  Wed, 11 Apr 2012 11:19:27 +0100

libfiu (0.90-2) unstable; urgency=low

  * Apply patch from upstream to attempt to fix broken builds.
    <http://blitiri.com.ar/git/?p=libfiu;a=commit;h=005a56fbcb5ab8045f9d51676c521b335f058552>

 -- Chris Lamb <lamby@debian.org>  Fri, 06 Apr 2012 19:03:08 +0100

libfiu (0.90-1) unstable; urgency=low

  * New upstream release.
  * Add python3-dev to Build-Depends.
  * Add patch from upstream to fix Python tests.
    <http://blitiri.com.ar/git/?p=libfiu;a=commit;h=83fb1e675de26ad5409e554e2947e53c8f1b10c0>
  * Update symbols.

 -- Chris Lamb <lamby@debian.org>  Thu, 05 Apr 2012 09:26:02 +0100

libfiu (0.14-2) unstable; urgency=low

  * Add missing Build-Depends on python-support. (Closes: #642450)

 -- Chris Lamb <lamby@debian.org>  Wed, 16 Nov 2011 17:38:10 +0000

libfiu (0.14-1) unstable; urgency=low

  * New upstream release.
    - Drop "01_libc6_location.diff"; applied upstream.
    - Drop "02_mode_t_freebsd.diff"; applied upstream.
  * Update Vcs-{Git,Browser}.
  * Bump Standards-Version to 3.9.1.
  * Bump Standards-Version to 3.9.2.

 -- Chris Lamb <lamby@debian.org>  Fri, 15 Jul 2011 16:19:14 +0100

libfiu (0.13-3) unstable; urgency=low

  * Normalise override disparity by moving fiu-utils from "libdevel" to "devel"
  * Add patch to fix run-time abort on *BSD platforms. (Closes: #574752)

 -- Chris Lamb <lamby@debian.org>  Mon, 22 Mar 2010 07:22:19 +0000

libfiu (0.13-2) unstable; urgency=low

  * Add patch from upstream to determine location of libc.so.6 dynamically due
    to being named differently on IA64 and alpha. (Closes: #574405)
  * Bump Standards-Version to 3.8.4.

 -- Chris Lamb <lamby@debian.org>  Thu, 18 Mar 2010 10:41:03 +0000

libfiu (0.13-1) unstable; urgency=low

  * New upstream release.
  * Drop 01-add-version-to-Makefile.diff, 02-add-soname.diff,
    03-manpage-hyphens.diff, 04-manpage-dir.diff, 05-destdir-pkgconfig.diff,
    and 06-plibpath.diff: all applied upstream.
  * Bump Standards-Version to 3.8.3.
  * Bump Build-Depends on debhelper to 7.5.0~ as override functionality used.

 -- Chris Lamb <lamby@debian.org>  Sun, 18 Oct 2009 23:25:46 +0100

libfiu (0.12-1) unstable; urgency=low

  * Initial release. (Closes: #536732)

 -- Chris Lamb <lamby@debian.org>  Sat, 25 Jul 2009 12:43:58 +0200
