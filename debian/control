Source: libfiu
Section: libs
Priority: optional
Maintainer: Chris Lamb <lamby@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 libpython3-all-dev,
 python3-all-dev:any,
 python3-distutils,
 python3-docutils,
Standards-Version: 4.6.0
Homepage: https://blitiri.com.ar/p/libfiu/
Vcs-Git: https://salsa.debian.org/lamby/pkg-libfiu.git
Vcs-Browser: https://salsa.debian.org/lamby/pkg-libfiu
Rules-Requires-Root: no

Package: libfiu0
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: userspace fault injection framework
 libfiu is a C library for fault injection. It provides functions to mark
 "points of failure" inside your code (the "core API"), and functions to
 enable/disable the failure of those points (the "control API").
 .
 This package contains the libfiu shared library. To perform fault injection,
 you will require the "libfiu-dev" package.

Package: fiu-utils
Section: devel
Architecture: any
Depends:
 libfiu0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: userspace fault injection framework (utilities)
 libfiu is a C library for fault injection. It provides functions to mark
 "points of failure" inside your code (the "core API"), and functions to
 enable/disable the failure of those points (the "control API").
 .
 This package contains the fiu-run, fiu-ls and fiu-ctrl utilities.

Package: libfiu-dev
Section: libdevel
Architecture: any
Depends:
 libfiu0 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: userspace fault injection framework (development libraries)
 libfiu is a C library for fault injection. It provides functions to mark
 "points of failure" inside your code (the "core API"), and functions to
 enable/disable the failure of those points (the "control API").
 .
 This package contains the development libraries and documentation in HTML and
 reStructuredText formats.

Package: python3-fiu
Section: python
Architecture: any
Depends:
 libfiu0 (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Description: userspace fault injection framework (Python 3 bindings)
 libfiu is a C library for fault injection. It provides functions to mark
 "points of failure" inside your code (the "core API"), and functions to
 enable/disable the failure of those points (the "control API").
 .
 This package contains Python 3 bindings to libfiu.
