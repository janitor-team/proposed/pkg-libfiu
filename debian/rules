#!/usr/bin/make -f

DOCS = $(patsubst %.rst,%.html, $(wildcard doc/*.rst))
PYTHON_VERSIONS := $(shell py3versions -sv)
PYTHON_VERSION_DEFAULT := $(shell py3versions -dv)
DPKG_EXPORT_BUILDFLAGS = 1

export DEB_BUILD_MAINT_OPTIONS = hardening=+all

include /usr/share/dpkg/buildflags.mk

%:
	dh $@ --with python3

clean:
	rm -f doc/*.html

doc/%.html: doc/%.rst
	rst2html $< > $@

override_dh_auto_build: $(DOCS)
	dh_auto_build -- V=1
	cd bindings/python && \
	set -e && for i in $(PYTHON_VERSIONS); do \
		python$$i ./setup.py build; \
	done

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	find bindings/python/build \
		-maxdepth 1 \
		-type d \
		-not -name '*$(PYTHON_VERSION_DEFAULT)*' -print0 | \
		xargs -0r rm -rvf
	# Remove non-deterministic tests
	rm -fv tests/test-parallel.c tests/test-parallel-wildcard.c
	dh_auto_test -- V=1 LC_ALL=C
endif

override_dh_auto_install:
	make install PREFIX=$(CURDIR)/debian/tmp/usr DESTDIR=/usr PLIBPATH=/usr/lib/fiu V=1
	cd bindings/python && \
	set -e && for i in $(PYTHON_VERSIONS); do \
		python$$i ./setup.py install --root=$(CURDIR)/debian/python3-fiu --no-compile -O0 --install-layout=deb; \
	done

override_dh_compress:
	dh_compress -X.rst
